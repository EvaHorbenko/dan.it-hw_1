        /* Технический вопрос:
    Для работы с input не рекомендуется использовать события клавиатуры, так как в input можно вставить текст без использования клавиатуры, с помощью мыши. Ни одно событие клавиатуры при этом не произойдет. 
    При этом если написана функция, например, для проверки вводимых значений при каждом действии клавиатуры - проверка не произойдет вообще. */

const btn = document.querySelectorAll('.btn')

const highlightBtn = function (event) {
    let keyCode = event.code;
    for (let i = 0; i < btn.length; i++) {
        btn[i].dataset.key === keyCode ? btn[i].classList.add('active') : btn[i].classList.remove('active');
    }
}

document.addEventListener('keydown', highlightBtn);