function getNum (counter, operand) {
    let number;
    do {
        number = prompt(`Enter ${counter} number`);
        operand = Number(number)
    } while (Number.isNaN(operand) || operand !== parseInt(operand))

    return operand
}

function startFunction () {
    let firstOperand = getNum(`first`);
    let secondOperand = getNum(`second`);
    let stepCountNum = getNum(`count`);

    countFibonachiNums (firstOperand, secondOperand, stepCountNum)
}

        /*   L O N G E R    W A Y    T O    D O    T H E    S A M E   */

/* function startFunction (firstOperand, secondOperand, stepCountNum) {
    firstOperand = getFirstNum(firstOperand);
    secondOperand = getSecondNum(secondOperand);
    stepCountNum = getStepNum(stepCountNum);

    countFibonachiNums (firstOperand, secondOperand, stepCountNum)
}

function getFirstNum (firstOperand) {
    let firstNumber
    do {
        firstNumber = prompt('Enter first number', firstNumber || 0);
        firstOperand = Number(firstNumber)
    } while (Number.isNaN(firstOperand) || firstOperand !== parseInt(firstOperand))

    return firstOperand
}

function getSecondNum (secondOperand) {
    let secondNumber;
    do {
        secondNumber = prompt('Enter second number', secondNumber || 1);
        secondOperand = Number(secondNumber)
    } while (Number.isNaN(secondOperand) || secondOperand !== parseInt(secondOperand))

    return secondOperand
}

function getStepNum (stepCountNum) {
    let stepCount;
    do {
        stepCount = prompt('Enter count numbers', stepCount || 5);
        stepCountNum = Number(stepCount);
    } while (Number.isNaN(stepCountNum) || stepCountNum !== parseInt(stepCountNum))

    return stepCountNum
} */

function countFibonachiNums (firstOperand, secondOperand, stepCountNum) {
    if (firstOperand >= 0 && secondOperand > firstOperand || firstOperand <=0 && secondOperand < firstOperand) {
        let a = firstOperand;
        let b = secondOperand;
        let stepA = b - a;
        let result = b + a;
        let stepB = result - b;
    
        console.log(firstOperand)
        console.log(secondOperand)
        
        for (let i = 0; i <= stepCountNum; i++) {
            console.log(result)
            a += stepA;
            b += stepB;
            stepA = b - a;
            result = a + b;
            stepB = result - b;
        }
    } else {
        alert('If first number > or = 0, second number need to be > first number. \nIf first number < or = 0, second number need to be < first number.')
        startFunction()
    }
}

startFunction()