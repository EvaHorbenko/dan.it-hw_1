/*          Технический вопрос:
    цикл forEach перебирает все элементы массива, и производит указаные действия с каждым элементом */

let arrayForChecking = [41, undefined, 'string', true, 'true', 98.3, null, {firstName : 'John', secondName : 'Doe'}, undefined, '17', false, 'array', -14.1, null, -19, 'object', undefined, {age : 21}];

                                                /* for */
/* function filterBy (array, dataType) {
    let checkedArray = [];
    for (let elem of array) {
        if (typeof(elem) !== dataType) {
            checkedArray.push(elem)
        }
    }
    return checkedArray
} */

                                                /* forEach */
/* function filterBy (array, dataType) {
    let checkedArray = [];
    array.forEach(function (elem) {
        if (typeof(elem) !== dataType) {
            checkedArray.push(elem)
        }
    })
    return checkedArray
} */
                                                /* forEach => arrow function */
/* function filterBy (array, dataType) {
    let checkedArray = [];
    array.forEach(elem => typeof(elem) !== dataType ? checkedArray.push(elem) : checkedArray)
    return checkedArray
} */

                                                /* filter => arrow function */
let filterBy = (array, dataType) => array.filter(elem => typeof(elem) !== dataType)

const typesList = ['number', 'string', 'boolean', 'undefined', 'object'];

typesList.forEach(type => {
    let nonCurrentType = filterBy(arrayForChecking, type);
    console.log(`Non-${type}: `, nonCurrentType);
});