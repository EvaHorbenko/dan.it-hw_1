const form = document.querySelector('.password-form');

function showHidePassword (event) {
    const target = event.target;

    if (target.classList.contains('icon-password')) {
        const parent = target.closest('.input-wrapper');
        const input = parent.querySelector('.input');
        input.setAttribute('type', input.type === 'password' ? 'text' : 'password')

        const icon = parent.querySelector('.icon-password');
        icon.classList.toggle('fa-eye');
        icon.classList.toggle('fa-eye-slash');
    }
}

function checkForm (event) {
    event.preventDefault();
    /* при сабмите формы происходит отправка данных на сервер, а нам их нужно сначала проверить. Поэтому необходимо прописать event.preventDefault() */
    
    const mainPassword = form.querySelector('.main-password');
    const repeatePassword = form.querySelector('.repeate-password');
    const alertMessage = form.querySelector('.alert-message');
    if (mainPassword.value === repeatePassword.value && mainPassword.value !== '') {
        alertMessage.classList.add('hidden');
        alert('You are welcome');
    } else {
        alertMessage.classList.remove('hidden');
    }
}

form.addEventListener('click', showHidePassword);
form.addEventListener('submit', checkForm);