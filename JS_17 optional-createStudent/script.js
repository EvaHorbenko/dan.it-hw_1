const student = {
    firstName: `${prompt("Enter student's first name")}`,
    lastName: `${prompt("Enter student's last name")}`,
    tabel: {},
}

let subject;
let score;

do {
    subject = prompt('Enter subject name');
    if (subject !== null && subject !== '') {
        do {
            score = Number(prompt(`Enter ${subject} score`));
        } while (isNaN(score) || score <= 0 || score !== parseInt(score) || score > 10)
        student.tabel[subject] = score;
    }
} while (subject !== null)

let tabelValues = Object.values(student.tabel)
let scoreSum  = tabelValues.reduce((sum, current) => sum + current)
let average = scoreSum / tabelValues.length;

tabelValues.find(badScore => badScore < 4) ? alert(`Student ${student.firstName} ${student.lastName} left for the second year.`) : alert(`Student ${student.firstName} ${student.lastName} proceeds to the next course.`)

alert(`${student.firstName} ${student.lastName} received an average ${average}`)

if (average > 7) alert(`Student ${student.firstName} ${student.lastName} assigned a scholarship.`);