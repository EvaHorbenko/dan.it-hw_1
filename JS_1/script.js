/*             Технический вопрос: 
1.  let - переменная, ее можно перезаписать - задать ей другое, отличное от предыдущего значение;
    const - константа - в нее можно записать значение, которое потом нельзя будет перезаписать;
2.  var - устаревшее обозначение переменной, ее так же можно перезаписать, но она отличается от let. var игнорирует блоки, переменную можно     вызвать вне блока, в котором она инициализирована. В случае с let это невозможно. 
    Переменные заданые через var обрабатываются в начале функции/скрипта - ее можно вызвать до инициализации */

let name = prompt("Enter your name");
if(name.length < 3 || name.length > 30) {
    name = prompt("Enter your real name", name)
}

let age = prompt("Enter your age");
let ageNum = Number(age)

if (Number.isNaN(ageNum) == true) {
    ageNum = Number(prompt("Enter your real age using numbers", age))
}

if (ageNum < 18) {
    alert("You are not allowed to visit this website")
} else if (ageNum >= 18 && ageNum <= 22) {
    const isContinue = confirm("Are you sure you want to continue?")
    if (isContinue) {
        alert(`Welcome, ${name}`)
    } else {
        alert("You are not allowed to visit this website")
    }
} else {
    alert(`Welcome, ${name}`)
}