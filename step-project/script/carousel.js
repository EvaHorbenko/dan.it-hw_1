const sayAbout = document.querySelector('.say-about');
const caruoselWrapper = sayAbout.querySelector('.caruosel-wrapper');
const commentBlocks = sayAbout.querySelectorAll('.comment-block');
const otherHuman = sayAbout.querySelector('.other-human');
const humanImgs = otherHuman.querySelectorAll('.human-img');

const commentBlockWidth = commentBlocks[0].offsetWidth;

let humanCountNumber = 0;

const setValues = function () {
    commentBlocks[0].style.marginLeft = `${-humanCountNumber * commentBlockWidth}px`;
    commentBlocks[0].style.transition = `500ms`;
}

const setActive = function () {
    let humanIndex = humanCountNumber % commentBlocks.length
    for (let humanImg of humanImgs) {
        humanImg.classList.remove('active');
    }
    humanImgs[humanIndex].classList.add('active')
}

const choseHuman = function (event) {
    let target = event.target;
    if (target.closest('.backwards')) {
        humanCountNumber--;
        humanCountNumber < 0 ? humanCountNumber = commentBlocks.length - 1 : humanCountNumber;
        setValues();
        setActive();
    } else if (target.closest('.forwards')) {
        humanCountNumber++;
        humanCountNumber > commentBlocks.length - 1 ? humanCountNumber = 0 : humanCountNumber;
        setValues();
        setActive();
    } else if (target.closest('.human-img')) {
        humanCountNumber = target.closest('.human-img').dataset.humanNumber;

        setValues();
        setActive();
    }
}

otherHuman.addEventListener('click', choseHuman)