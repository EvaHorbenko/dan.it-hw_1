const ourWork = document.querySelector('.our-work');
const loadMoreBtn = ourWork.querySelector('#load_more-btn');
const workItems = ourWork.querySelectorAll('.work-item');
const worksItemsWrapper = ourWork.querySelector('.works-items-wrapper');
const workItemNames = ourWork.querySelectorAll('.work-item-name');

let isLoadedMore = false;

const cleenFilter = function () {
    for (let i= 0; i < workItemNames.length; i++) {
        workItemNames[i].classList.remove('active');
    }
    workItemNames[0].classList.add('active');
}

const loadMore = function () {
    cleenFilter();
    for (let workItem of workItems) {
        workItem.classList.remove('hidden');
    }
    loadMoreBtn.style.display = 'none';
    isLoadedMore = true;
}

loadMoreBtn.addEventListener('click', loadMore)


const filterWorkItems = function (event) {
    let target = event.target
    for (let workItemName of workItemNames) {
        workItemName.classList.remove('active')
    }
    target.classList.add('active');

    if (isLoadedMore) {
        for (let workItem of workItems) {
            filterByCase(target, workItem)
        }
    } else {    // !isLoadedMore
        for (let i = 0; i < 12; i++) {
            filterByCase(target, workItems[i])
        }
    }
}

const filterByCase = function (target, elemForFilter) {
    elemForFilter.classList.add('hidden');
    if (target.dataset.workName === elemForFilter.dataset.workName) {
        elemForFilter.classList.remove('hidden');
    } else if (target.dataset.workName === 'All') {
        elemForFilter.classList.remove('hidden');
    }
}

worksItemsWrapper.addEventListener('click', filterWorkItems)