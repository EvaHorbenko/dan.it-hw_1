const HUMANS_AND_COMMENTS = {
    0: {
        comment: 'It was many and many a year ago, In a kingdom by the sea, That a maiden there lived whom you may know By the name of Annabel Lee; And this maiden she lived with no other thought Than to love and be loved by me.',
        fullName: 'Annabel Lee',
        position: 'Business Intelligence',
        imgSrc: './img/comments/human1.png'
    },
    1: {
        comment: 'Once upon a midnight dreary, while I pondered, weak and weary, Over many a quaint and curious volume of forgotten lore - While I nodded, nearly napping, suddenly there came a tapping, As of some one gently rapping, rapping at my chamber door.',
        fullName: 'Edgar Allan Poe',
        position: 'Software Engineering: CI/CD',
        imgSrc: './img/comments/human2.png'
    },
    2: {
        comment: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
        fullName: 'Hasan Ali',
        position: 'UX Designer',
        imgSrc: './img/comments/comment-3.png'
    },
    3: {
        comment: 'Ah broken is the golden bowl! the spirit flown forever! Let the bell toll!--a saintly soul floats on the Stygian river; And, Guy De Vere, hast thou no tear?--weep now or never more! See! on yon drear and rigid bier low lies thy love, Lenore!',
        fullName: 'Lenore',
        position: 'Full Stack',
        imgSrc: './img/comments/human4.png'
    },
}

const sayAbout = document.querySelector('.say-about');
const commentBlock = sayAbout.querySelector('.comment-block');

const comment = commentBlock.querySelector('.comment');
const humanName = commentBlock.querySelector('.full-name');
const humanPosition = commentBlock.querySelector('.position');
const photo = commentBlock.querySelector('.photo');

const otherHuman = sayAbout.querySelector('.other-human');
const humanImgs = otherHuman.querySelectorAll('.human-img');
console.log(humanImgs)

let humanCountNumber = 2;


const setValues = function () {
    comment.innerHTML = HUMANS_AND_COMMENTS[humanCountNumber].comment;
    humanName.innerHTML = HUMANS_AND_COMMENTS[humanCountNumber].fullName;
    humanPosition.innerHTML = HUMANS_AND_COMMENTS[humanCountNumber].position;
    photo.src = HUMANS_AND_COMMENTS[humanCountNumber].imgSrc;
}

const setActive = function () {
    for (let humanImg of humanImgs) {
        humanImg.classList.remove('active');
    }
    humanImgs[humanCountNumber].classList.add('active')
}

const choseHuman = function (event) {
    let target = event.target;
    if (target.closest('.backwards')) {
        humanCountNumber--;
        humanCountNumber < 0 ? humanCountNumber = Object.keys(HUMANS_AND_COMMENTS).length - 1 : humanCountNumber;
        setValues();
        setActive();
    } else if (target.closest('.forwards')) {
        humanCountNumber++;
        humanCountNumber > Object.keys(HUMANS_AND_COMMENTS).length - 1 ? humanCountNumber = 0 : humanCountNumber;
        setValues();
        setActive();
    } else if (target.closest('.human-img')) {
        humanCountNumber = target.closest('.human-img').dataset.humanNumber;
        setValues();
        setActive();
    }
}

otherHuman.addEventListener('click', choseHuman)
