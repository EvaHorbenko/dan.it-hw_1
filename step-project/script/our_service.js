const SERVICE = {
    'Web Design': {
        imgSrc : './img/service/service1.png',
        description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    'Graphic Design': {
        imgSrc : './img/service/service2.jpg',
        description : 'The escape of the Brazilian boa constrictor earned Harry his longest-ever punishment. By the time he was allowed out of his cupboard again, the summer holidays had started and Dudley had already broken his new video camera, crashed his remote control airplane, and, first time out on his racing bike, knocked down old Mrs. Figg as she crossed Privet Drive on her crutches. Harry was glad school was over, but there was no escaping Dudley’s gang, who visited the house every single day. Piers, Dennis, Malcolm, and Gordon were all big and stupid, but as Dudley was the biggest and stupidest of the lot, he was the leader. The rest of them were all quite happy to join in Dudley’s favorite sport: Harry Hunting.'
    }, 
    'Online Support': {
        imgSrc : './img/service/service3.jpg',
        description : 'Hagrid was so huge that he parted the crowd easily; all Harry had to do was keep close behind him. They passed book shops and music stores, hamburger restaurants and cinemas, but nowhere that looked as if it could sell you a magic wand. This was just an ordinary street full of ordinary people. Could there really be piles of wizard gold buried miles beneath them? Were there really shops that sold spell books and broomsticks? Might this not all be some huge joke that the Dursleys had cooked up? If Harry hadn’t known that the Dursleys had no sense of humor, he might have thought so; yet somehow, even though everything Hagrid had told him so far was unbelievable, Harry couldn’t help trusting him.'
    }, 
    'App Design': {
        imgSrc : './img/service/service4.jpg',
        description : 'Harry heard the hat shout the last word to the whole hall. He took off the hat and walked shakily toward the Gryffindor table. He was so relieved to have been chosen and not put in Slytherin, he hardly noticed that he was getting the loudest cheer yet. Percy the Prefect got up and shook his hand vigorously, while the Weasley twins yelled, “We got Potter! We got Potter!” Harry sat down opposite the ghost in the ruff he’d seen earlier. The ghost patted his arm, giving Harry the sudden, horrible feeling he’d just plunged it into a bucket of ice-cold water. He could see the High Table properly now. At the end nearest him sat Hagrid, who caught his eye and gave him the thumbs up. Harry grinned back.'
    }, 
    'Online Marketing': {
        imgSrc : './img/service/service5.jpg',
        description : 'He could see the High Table properly now. At the end nearest him sat Hagrid, who caught his eye and gave him the thumbs up. Harry grinned back. And there, in the center of the High Table, in a large gold chair, sat Albus Dumbledore. Harry recognized him at once from the card he’d gotten out of the Chocolate Frog on the train. Dumbledore’s silver hair was the only thing in the whole hall that shone as brightly as the ghosts. Harry spotted Professor Quirrell, too, the nervous young man from the Leaky Cauldron. He was looking very peculiar in a large purple turban. And now there were only three people left to be sorted. “Thomas, Dean” a Black boy even taller than Ron, joined Harry at the Gryffindor table.'
    }, 
    'Seo Service': {
        imgSrc : './img/service/service6.jpg',
        description : 'The Gryffindor first years followed Percy through the chattering crowds, out of the Great Hall, and up the marble staircase. Harry’s legs were like lead again, but only because he was so tired and full of food. He was too sleepy even to be surprised that the people in the portraits along the corridors whispered and pointed as they passed, or that twice Percy led them through doorways hidden behind sliding panels and hanging tapestries. They climbed more staircases, yawning and dragging their feet, and Harry was just wondering how much farther they had to go when they came to a sudden halt. A bundle of walking sticks was floating in midair ahead of them, and as Percy took a step toward them they started throwing themselves at him. “Peeves,” Percy whispered to the first years.'
    }, 
};

const serviceItemsWrapper = document.querySelector('.service-items-wrapper');
const serviceItems = document.querySelectorAll('.service-item');

const serviceDescription = document.querySelector('.service-description');
let descriptionImg = serviceDescription.querySelector('.description-img');
let descriptionInfo = serviceDescription.querySelector('.description-info');

serviceItemsWrapper.addEventListener('click', function (event) {
    let target = event.target;
    let targetData = target.dataset.service;
    for (let i = 0; i < Object.keys(SERVICE).length; i++) {
        if (targetData === Object.keys(SERVICE)[i]) {
            descriptionImg.src = SERVICE[targetData].imgSrc;
            descriptionInfo.innerHTML = SERVICE[targetData].description;
        }
    }
    for (let i = 0; i < serviceItems.length; i++) {
        serviceItems[i].classList.remove('active');
    }
    target.classList.add('active');
})
