const changeThemeBtn = document.querySelector('.channgeTheme-btn');
let isLigthTheme;
let LSTheme = window.localStorage.getItem('LigthTheme');

const changeTheme = function (event) {
    let clicked = Boolean(event);

    if (clicked) {
        document.body.classList.toggle('light-theme');
        document.body.classList.toggle('dark-theme');
        isLigthTheme = document.body.classList.contains('light-theme');
    } else {
        if (LSTheme === 'light') {
            document.body.classList.add('light-theme');
            document.body.classList.remove('dark-theme');
            isLigthTheme = true;
        } else {
            document.body.classList.remove('light-theme');
            document.body.classList.add('dark-theme');
            isLigthTheme = false;
        }
    }
}
changeTheme();

changeThemeBtn.addEventListener('click', changeTheme);
window.addEventListener('beforeunload', function () {
    window.localStorage.setItem('LigthTheme', isLigthTheme ? 'light' : 'dark');
})