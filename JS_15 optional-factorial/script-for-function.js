/*             Технический вопрос: 
рекурсия - математическая функция "вложенная" сама в себя - в процессе отработки обращается сама к себе. */

function getNum (number) {
    let operand;
    number = prompt('Enter number', number);
    operand = Number(number);
    checkNum(number, operand);
}

function checkNum (number, operand) {
    operand = Number.isNaN(operand) || operand < 1 ? getNum(number) : countFactorial(operand);
}

function countFactorial (operand) {
    let result = 1;
    for (let i = operand; i > 0; i--) {
        result *= i;
    }
    showResult(result, operand);
}

function showResult (result, operand) {
    console.log(`${operand}! = ${result}`);
}

getNum();